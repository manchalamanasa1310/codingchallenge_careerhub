--1. Provide a SQL script that initializes the database for the Job Board scenario “CareerHub”.
use CareerHub
--2. Create tables for Companies, Jobs, Applicants and Applications.
--3. Define appropriate primary keys, foreign keys, and constraints.
--4. Ensure the script handles potential errors, such as if the database or tables already exist.

create table Companies(
CompanyID int primary key,
CompanyName varchar(50),
Location varchar(100)
)
insert into Companies values
(1,'Hexaware','Chennai'),
(2,'Google','Banglore')

create table Jobs(
JobID int primary key,
CompanyID int,
JobTitle varchar(50),
JobDescription text,
JobLocation varchar(100),
Salary decimal,
JobType varchar(50),
PostedDate datetime,
foreign key(CompanyID) references Companies(CompanyID)
)

insert into Jobs values
(1,1,'Developer','Develops the applications','Hyderabad',100000,'Full-Time','2023-12-12'),
(2,2,'Tester','Tests the applications','Hyderabad',100000,'Full_Time','2023-12-11')

create table Applicants(
ApplicantID int primary key,
FirstName varchar(50),
LastName varchar(50),
Email varchar(50),
Phone varchar(50),
Resume text
)
insert into Applicants values
(1,'Manasa','Manchala','manasa@gmail.com','1234567223','resume file'),
(2,'Swarupa','Manchala','swarupa@gmail.com','1452542411','resume file')

create table Applications(
ApplicationID int primary key,
JobID int,
ApplicantID int,
ApplicationDate datetime,
CoverLetter text,
foreign key(JobID) references Jobs(JobID),
foreign key(ApplicantID) references Applicants(ApplicantID)
)

insert into Applications values
(1,1,1,'2023-12-12','application for sde'),
(2,2,2,'2023-12-11','application for tester')

--5. Write an SQL query to count the number of applications received for each job listing in the
--"Jobs" table. Display the job title and the corresponding application count. Ensure that it lists all
--jobs, even if they have no applications.
select Jobs.JobID,count(*) as NumberofApplications from Jobs left join Applications on Jobs.JobID=Applications.JobID
group by Jobs.JobID

--6. Develop an SQL query that retrieves job listings from the "Jobs" table within a specified salary
--range. Allow parameters for the minimum and maximum salary values. Display the job title,
--company name, location, and salary for each matching job.
declare @minsalary decimal
declare @maxsalary decimal
set @minsalary=60000
set @maxsalary=120000
select Jobs.JobID,Companies.CompanyID,Jobs.JobLocation,Jobs.Salary from Jobs join Companies
on Jobs.CompanyID=Companies.CompanyID where Jobs.Salary between @minsalary and @maxsalary

--7. Write an SQL query that retrieves the job application history for a specific applicant. Allow a
--parameter for the ApplicantID, and return a result set with the job titles, company names, and
--application dates for all the jobs the applicant has applied to.
declare @appid int
set @appid=1
select Jobs.JobTitle,Companies.CompanyID,Applications.ApplicationDate from Jobs join Companies
on Jobs.CompanyID=Companies.CompanyID join Applications
on Jobs.JobID=Applications.JobID where Applications.ApplicationID=@appid

--8. Create an SQL query that calculates and displays the average salary offered by all companies for
--job listings in the "Jobs" table. Ensure that the query filters out jobs with a salary of zero.
select Companies.CompanyName,avg(Jobs.Salary) from Companies join Jobs on Jobs.CompanyID=Companies.CompanyID
where Jobs.Salary>0 group by Companies.CompanyName

--9. Write an SQL query to identify the company that has posted the most job listings. Display the
--company name along with the count of job listings they have posted. Handle ties if multiple
--companies have the same maximum count.
select CompanyName, max(NumofPosts) from
(select Companies.CompanyName,count(*) as NumofPosts from Companies join Jobs on Companies.CompanyID=Jobs.CompanyID
group by Companies.CompanyName) as subquery
group by CompanyName

--10. Find the applicants who have applied for positions in companies located in 'CityX' and have at
--least 3 years of experience.
select Applicants.ApplicantID from Applicants
join Applications on Applicants.ApplicantID = Applications.ApplicantID
join Jobs on Applications.JobID = Jobs.JobID
join Companies on Jobs.CompanyID = Companies.CompanyID
where Companies.Location = 'Chennai'
and datediff(year, Applications.ApplicationDate, getdate()) >= 3

--11. Retrieve a list of distinct job titles with salaries between $60,000 and $80,000.
select distinct JobTitle from Jobs where Salary between 60000 and 80000

--12. Find the jobs that have not received any applications.
select JobID from Jobs where JobID not in (select JobID from Applications)

--13. Retrieve a list of job applicants along with the companies they have applied to and the positions
--they have applied for.
select Applicants.ApplicantID, Companies.CompanyName, Jobs.JobTitle from Applicants
join Applications on Applicants.ApplicantID = Applications.ApplicantID
join Jobs on Applications.JobID = Jobs.JobID
join Companies on Jobs.CompanyID = Companies.CompanyID

--14. Retrieve a list of companies along with the count of jobs they have posted, even if they have not
--received any applications.
select Companies.CompanyName,count(*) from Companies left join Jobs on Companies.CompanyID=Jobs.CompanyID
group by Companies.CompanyName

--15. List all applicants along with the companies and positions they have applied for, including those
--who have not applied.
select Applicants.ApplicantID from Applicants left join Applications on Applicants.ApplicantID=Applications.ApplicantID
left join Jobs on Applications.JobID=Jobs.JobID
left join Companies on Jobs.CompanyID=Companies.CompanyID

--16. Find companies that have posted jobs with a salary higher than the average salary of all jobs.
select Companies.CompanyID from Companies join Jobs on Companies.CompanyID=Jobs.CompanyID
where Jobs.Salary>(select avg(Jobs.Salary) from Jobs)

--17. Display a list of applicants with their names and a concatenated string of their city and state.
--select FistName,LastName, concat(
--18. Retrieve a list of jobs with titles containing either 'Developer' or 'Engineer'.
select JobID,JobTitle from Jobs where JobTitle='Developer' or JobTitle='Engineer'

--19. Retrieve a list of applicants and the jobs they have applied for, including those who have not
--applied and jobs without applicants.
select Applicants.ApplicantID, Companies.CompanyName, Jobs.JobTitle from Applicants
full outer join Applications on Applicants.ApplicantID = Applications.ApplicantID
full outer join Jobs on Applications.JobID = Jobs.JobID
full outer join Companies on Jobs.CompanyID = Companies.CompanyID

--20. List all combinations of applicants and companies where the company is in a specific city and the
--applicant has more than 2 years of experience. For example: city=Chennai
select Applicants.ApplicantID, Companies.CompanyName from Applicants
join Applications on Applicants.ApplicantID = Applications.ApplicantID
join Jobs on Applications.JobID = Jobs.JobID
join Companies on Jobs.CompanyID = Companies.CompanyID
where Companies.Location = 'Chennai' and datediff(year, Applications.ApplicationDate, getdate()) > 2
